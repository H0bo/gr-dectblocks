#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Top Block
# Generated: Wed Jul 19 22:30:10 2017
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from PyQt4.QtCore import QObject, pyqtSlot
from gnuradio import audio
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio import vocoder
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from gnuradio.qtgui import Range, RangeWidget
from optparse import OptionParser
import dect2
import dectblocks
import sys
import threading
import time


class top_block(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Top Block")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Top Block")
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "top_block")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())

        ##################################################
        # Variables
        ##################################################
        self.dect_symbol_rate = dect_symbol_rate = 1152000
        self.dect_occupied_bandwidth = dect_occupied_bandwidth = 1.2*dect_symbol_rate
        self.dect_channel_bandwidth = dect_channel_bandwidth = 1.728e6
        self.baseband_sampling_rate = baseband_sampling_rate = 2.35e6
        self.rx_mode_chooser = rx_mode_chooser = 0
        self.rx_gain = rx_gain = 3
        self.rx_freq_func = rx_freq_func = 1897344000
        self.rx_chan = rx_chan = 1
        self.resampler_filter_taps = resampler_filter_taps = firdes.low_pass_2(1, 3*baseband_sampling_rate, dect_occupied_bandwidth/2, (dect_channel_bandwidth - dect_occupied_bandwidth)/2, 30)
        self.part_id = part_id = 9

        ##################################################
        # Blocks
        ##################################################
        self.probe_freq = blocks.probe_signal_i()
        self.vocoder_g721_decode_bs_0 = vocoder.g721_decode_bs()
        self._rx_mode_chooser_options = (0, 1, 2, )
        self._rx_mode_chooser_labels = ("Manuel", "Scan", "Listen", )
        self._rx_mode_chooser_group_box = Qt.QGroupBox("Choose mode")
        self._rx_mode_chooser_box = Qt.QVBoxLayout()
        class variable_chooser_button_group(Qt.QButtonGroup):
            def __init__(self, parent=None):
                Qt.QButtonGroup.__init__(self, parent)
            @pyqtSlot(int)
            def updateButtonChecked(self, button_id):
                self.button(button_id).setChecked(True)
        self._rx_mode_chooser_button_group = variable_chooser_button_group()
        self._rx_mode_chooser_group_box.setLayout(self._rx_mode_chooser_box)
        for i, label in enumerate(self._rx_mode_chooser_labels):
        	radio_button = Qt.QRadioButton(label)
        	self._rx_mode_chooser_box.addWidget(radio_button)
        	self._rx_mode_chooser_button_group.addButton(radio_button, i)
        self._rx_mode_chooser_callback = lambda i: Qt.QMetaObject.invokeMethod(self._rx_mode_chooser_button_group, "updateButtonChecked", Qt.Q_ARG("int", self._rx_mode_chooser_options.index(i)))
        self._rx_mode_chooser_callback(self.rx_mode_chooser)
        self._rx_mode_chooser_button_group.buttonClicked[int].connect(
        	lambda i: self.set_rx_mode_chooser(self._rx_mode_chooser_options[i]))
        self.top_layout.addWidget(self._rx_mode_chooser_group_box)
        self._rx_gain_range = Range(0, 30, 1, 3, 200)
        self._rx_gain_win = RangeWidget(self._rx_gain_range, self.set_rx_gain, "RX Gain", "counter_slider", float)
        self.top_layout.addWidget(self._rx_gain_win)
        def _rx_freq_func_probe():
            while True:
                val = self.probe_freq.level()
                try:
                    self.set_rx_freq_func(val)
                except AttributeError:
                    pass
                time.sleep(1.0 / (1000))
        _rx_freq_func_thread = threading.Thread(target=_rx_freq_func_probe)
        _rx_freq_func_thread.daemon = True
        _rx_freq_func_thread.start()
        self._rx_chan_options = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        self._rx_chan_labels = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        self._rx_chan_tool_bar = Qt.QToolBar(self)
        self._rx_chan_tool_bar.addWidget(Qt.QLabel("Carrier Number"+": "))
        self._rx_chan_combo_box = Qt.QComboBox()
        self._rx_chan_tool_bar.addWidget(self._rx_chan_combo_box)
        for label in self._rx_chan_labels: self._rx_chan_combo_box.addItem(label)
        self._rx_chan_callback = lambda i: Qt.QMetaObject.invokeMethod(self._rx_chan_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._rx_chan_options.index(i)))
        self._rx_chan_callback(self.rx_chan)
        self._rx_chan_combo_box.currentIndexChanged.connect(
        	lambda i: self.set_rx_chan(self._rx_chan_options[i]))
        self.top_layout.addWidget(self._rx_chan_tool_bar)
        self.rational_resampler_xxx_0 = filter.rational_resampler_fff(
                interpolation=6,
                decimation=1,
                taps=None,
                fractional_bw=None,
        )
        self.rational_resampler = filter.rational_resampler_base_ccc(3, 2, (resampler_filter_taps))
        self._part_id_options = [9, 0, 1, 2, 3, 4, 5, 6, 7, 8]
        self._part_id_labels = ["none","0", "1", "2", "3", "4", "5", "6", "7", "8"]
        self._part_id_tool_bar = Qt.QToolBar(self)
        self._part_id_tool_bar.addWidget(Qt.QLabel("Select Part"+": "))
        self._part_id_combo_box = Qt.QComboBox()
        self._part_id_tool_bar.addWidget(self._part_id_combo_box)
        for label in self._part_id_labels: self._part_id_combo_box.addItem(label)
        self._part_id_callback = lambda i: Qt.QMetaObject.invokeMethod(self._part_id_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._part_id_options.index(i)))
        self._part_id_callback(self.part_id)
        self._part_id_combo_box.currentIndexChanged.connect(
        	lambda i: self.set_part_id(self._part_id_options[i]))
        self.top_layout.addWidget(self._part_id_tool_bar)
        self.fractional_resampler = filter.fractional_resampler_cc(0, (3.0*baseband_sampling_rate/2.0)/dect_symbol_rate/4.0)
        self.dectblocks_phase_diff_0 = dectblocks.phase_diff()
        self.dectblocks_packet_receiver_0 = dectblocks.packet_receiver()
        self.dectblocks_packet_decoder_0 = dectblocks.packet_decoder()
        self.console_0 = dect2.console()
        self.top_layout.addWidget(self.console_0)
          
        self.blocks_tag_debug_0 = blocks.tag_debug(gr.sizeof_char*1, "", ""); self.blocks_tag_debug_0.set_display(True)
        self.blocks_short_to_float_0 = blocks.short_to_float(1, 32768)
        self.blocks_message_debug_0 = blocks.message_debug()
        self.blocks_file_source_0 = blocks.file_source(gr.sizeof_gr_complex*1, "/home/felix/Schreibtisch/dect_call", True)
        self.audio_sink_0 = audio.sink(48000, "", True)

        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.dectblocks_packet_decoder_0, 'log_out'), (self.console_0, 'in'))    
        self.msg_connect((self.dectblocks_packet_receiver_0, 'rcvr_msg_out'), (self.blocks_message_debug_0, 'print'))    
        self.msg_connect((self.dectblocks_packet_receiver_0, 'rcvr_msg_out'), (self.dectblocks_packet_decoder_0, 'rcvr_msg_in'))    
        self.connect((self.blocks_file_source_0, 0), (self.rational_resampler, 0))    
        self.connect((self.blocks_short_to_float_0, 0), (self.rational_resampler_xxx_0, 0))    
        self.connect((self.dectblocks_packet_decoder_0, 1), (self.probe_freq, 0))    
        self.connect((self.dectblocks_packet_decoder_0, 0), (self.vocoder_g721_decode_bs_0, 0))    
        self.connect((self.dectblocks_packet_receiver_0, 0), (self.blocks_tag_debug_0, 0))    
        self.connect((self.dectblocks_packet_receiver_0, 0), (self.dectblocks_packet_decoder_0, 0))    
        self.connect((self.dectblocks_phase_diff_0, 0), (self.dectblocks_packet_receiver_0, 0))    
        self.connect((self.fractional_resampler, 0), (self.dectblocks_phase_diff_0, 0))    
        self.connect((self.rational_resampler, 0), (self.fractional_resampler, 0))    
        self.connect((self.rational_resampler_xxx_0, 0), (self.audio_sink_0, 0))    
        self.connect((self.vocoder_g721_decode_bs_0, 0), (self.blocks_short_to_float_0, 0))    

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "top_block")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()


    def get_dect_symbol_rate(self):
        return self.dect_symbol_rate

    def set_dect_symbol_rate(self, dect_symbol_rate):
        self.dect_symbol_rate = dect_symbol_rate
        self.set_dect_occupied_bandwidth(1.2*self.dect_symbol_rate)
        self.fractional_resampler.set_resamp_ratio((3.0*self.baseband_sampling_rate/2.0)/self.dect_symbol_rate/4.0)

    def get_dect_occupied_bandwidth(self):
        return self.dect_occupied_bandwidth

    def set_dect_occupied_bandwidth(self, dect_occupied_bandwidth):
        self.dect_occupied_bandwidth = dect_occupied_bandwidth
        self.set_resampler_filter_taps(firdes.low_pass_2(1, 3*self.baseband_sampling_rate, self.dect_occupied_bandwidth/2, (self.dect_channel_bandwidth - self.dect_occupied_bandwidth)/2, 30))

    def get_dect_channel_bandwidth(self):
        return self.dect_channel_bandwidth

    def set_dect_channel_bandwidth(self, dect_channel_bandwidth):
        self.dect_channel_bandwidth = dect_channel_bandwidth
        self.set_resampler_filter_taps(firdes.low_pass_2(1, 3*self.baseband_sampling_rate, self.dect_occupied_bandwidth/2, (self.dect_channel_bandwidth - self.dect_occupied_bandwidth)/2, 30))

    def get_baseband_sampling_rate(self):
        return self.baseband_sampling_rate

    def set_baseband_sampling_rate(self, baseband_sampling_rate):
        self.baseband_sampling_rate = baseband_sampling_rate
        self.set_resampler_filter_taps(firdes.low_pass_2(1, 3*self.baseband_sampling_rate, self.dect_occupied_bandwidth/2, (self.dect_channel_bandwidth - self.dect_occupied_bandwidth)/2, 30))
        self.fractional_resampler.set_resamp_ratio((3.0*self.baseband_sampling_rate/2.0)/self.dect_symbol_rate/4.0)

    def get_rx_mode_chooser(self):
        return self.rx_mode_chooser

    def set_rx_mode_chooser(self, rx_mode_chooser):
        self.rx_mode_chooser = rx_mode_chooser
        self._rx_mode_chooser_callback(self.rx_mode_chooser)
        self.dectblocks_packet_receiver_0.select_rx_mode(self.rx_mode_chooser, self.rx_freq_func, self.rx_chan, self.part_id)

    def get_rx_gain(self):
        return self.rx_gain

    def set_rx_gain(self, rx_gain):
        self.rx_gain = rx_gain

    def get_rx_freq_func(self):
        return self.rx_freq_func

    def set_rx_freq_func(self, rx_freq_func):
        self.rx_freq_func = rx_freq_func
        self.dectblocks_packet_receiver_0.select_rx_mode(self.rx_mode_chooser, self.rx_freq_func, self.rx_chan, self.part_id)

    def get_rx_chan(self):
        return self.rx_chan

    def set_rx_chan(self, rx_chan):
        self.rx_chan = rx_chan
        self.dectblocks_packet_receiver_0.select_rx_mode(self.rx_mode_chooser, self.rx_freq_func, self.rx_chan, self.part_id)
        self._rx_chan_callback(self.rx_chan)

    def get_resampler_filter_taps(self):
        return self.resampler_filter_taps

    def set_resampler_filter_taps(self, resampler_filter_taps):
        self.resampler_filter_taps = resampler_filter_taps
        self.rational_resampler.set_taps((self.resampler_filter_taps))

    def get_part_id(self):
        return self.part_id

    def set_part_id(self, part_id):
        self.part_id = part_id
        self._part_id_callback(self.part_id)
        self.dectblocks_packet_decoder_0.select_rx_part(self.part_id)
        self.dectblocks_packet_receiver_0.select_rx_mode(self.rx_mode_chooser, self.rx_freq_func, self.rx_chan, self.part_id)


def main(top_block_cls=top_block, options=None):

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
