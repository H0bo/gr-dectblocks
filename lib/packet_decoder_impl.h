/* -*- c++ -*- */
/* 
 * This code is based on Pavel Yazev's "gr-dect2" available at
 * https://github.com/pavelyazev/gr-dect2
 * 
 * 
 * Copyright 2015 Pavel Yazev <pyazev@gmail.com>
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_DECTBLOCKS_PACKET_DECODER_IMPL_H
#define INCLUDED_DECTBLOCKS_PACKET_DECODER_IMPL_H

#include <dectblocks/packet_decoder.h>

#define MAX_PARTS          8
#define A_FIELD_BITS      64
#define B_FIELD_BITS     320


namespace gr {
  namespace dectblocks {

    class packet_decoder_impl : public packet_decoder
    {

        private:            
            typedef enum {_RFP_, _PP_} part_type;

            typedef struct part_descriptor_item 
            {
                bool     active;
                bool     voice_present;
                bool     log_update;
                bool     part_id_rcvd;
                bool     qt_rcvd;

                bool     rpf_fn_cor;   // set true if frame number was corrected from RFP part 

                uint8_t  frame_number;
                uint64_t rx_seq;
                uint32_t rx_chan;       //channel
                uint8_t  part_id[5];
                part_type type;

                uint64_t packet_cnt;
                uint64_t afield_bad_crc_cnt;
                
                uint8_t  a_field_data[8];
                
                //static informations
                bool     staticinf_rcvd;
                bool     nr_bit;    //normal-reverse bit
                uint8_t  sn_bits;   //slot number bits
                uint8_t  sp_bits;   //start position bits  
                bool     esc_bit;   //wheter QT escapte is broadcast
                uint8_t  nrtrans_bits; //number of transceivers in RFP
                bool     extinf_bit;   //Extended carrier information available
                uint16_t carrier_bits; //which carriers are available
                uint8_t  carrier_number; //carrier number of transmission
                uint8_t  nextcarrier_number; //RF carrier on which one receiver will be listening on the next frame
                
                struct part_descriptor_item *pair;
            } part_descriptor_item;

            part_descriptor_item d_part_descriptor[MAX_PARTS];
            part_descriptor_item *d_cur_part;
            uint32_t d_selected_rx_id;
            uint32_t d_selected_rx_mode;  
            uint32_t d_change_to_rx_channel;
            
            uint32_t chan_to_freq[10];

            

            uint32_t decode_afield(uint8_t *field_data);

            int calculate_output_stream_length(const gr_vector_int &ninput_items);
            void msg_event_handler(pmt::pmt_t msg);

            void print_parts(void);
            void print_mode(void);

        public:            

            packet_decoder_impl();
            ~packet_decoder_impl();

            virtual void select_rx_part(uint32_t rx_id);

            int work(int noutput_items,
		            gr_vector_int &ninput_items,
		            gr_vector_const_void_star &input_items,
		            gr_vector_void_star &output_items);
    };

  } // namespace dectblocks
} // namespace gr

#endif /* INCLUDED_DECTBLOCKS_PACKET_DECODER_IMPL_H */

