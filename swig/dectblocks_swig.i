/* -*- c++ -*- */

#define DECTBLOCKS_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "dectblocks_swig_doc.i"

%{
#include "dectblocks/phase_diff.h"
#include "dectblocks/packet_decoder.h"
#include "dectblocks/packet_receiver.h"
%}

%include "dectblocks/phase_diff.h"
GR_SWIG_BLOCK_MAGIC2(dectblocks, phase_diff);


%include "dectblocks/packet_decoder.h"
GR_SWIG_BLOCK_MAGIC2(dectblocks, packet_decoder);
%include "dectblocks/packet_receiver.h"
GR_SWIG_BLOCK_MAGIC2(dectblocks, packet_receiver);
